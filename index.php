<?php
    include ("./includes/Autoload.inc.php");
    $products = new Query();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/bootstrap.css">
    <link rel="stylesheet" href="./css/custom.css">
    <script src="./js/bootstrap.js"></script>
    <title>Product-List</title>
</head>

<body>
    <div class="custom-nav-container">
        <div>
            <h1>Products List</h1>
        </div>
        <div>
            <a style="text-decoration: none;" href="addproduct.php">
                <button class="btn btn-primary">ADD</button>
            </a>
            <button form="product_form" type="submit" name="delete-submit" class="btn btn-danger">MASS DELETE</button>
        </div>
    </div>
    <form method="POST" action="post.process.php" id="product_form">
    <div class="custom-container">
        <div class="row">
        
        <?php 
            if($products->fetchProducts() > 0){
            foreach ($products->fetchProducts() as $item) { 
        ?>
                <div class="col-3">
                    <div style="margin-bottom: 10px; padding: 0px;" class="card">
                        <div style  class="card-body">
                            <input class="delete-checkbox" name="deleteCheckbox[]" type="checkbox" value="<?php echo $item['sku']?>">
                            <h4 class="text-center"><?php echo $item['sku']?></h4>
                            <p class="text-center"><?php echo $item['name']?></p>
                            <p class="text-center"><?php echo $item['price']?> $</p>
                            <p class="text-center">
                            <?php echo $item['attributeName'] ?> : <?php echo $item['attribute']?>
                            </p>
                        </div>
                    </div>
                </div>

        <?php 
            }} else {
                print_r('No Record Found');
            }
        ?>
        </div>
    </div>
    </form>
</body>

</html>