<?php
include ("./includes/Autoload.inc.php");
session_start();
    
    if(isset($_POST['submit'])){
        if(empty($_POST['productType'])){
            $_SESSION['InvalidInput'][] = 'Fields can not be empty <br>';
            header("location: addproduct.php");
        }
        else{
            $object = new $_POST['productType']($_POST);
            
                if($object->validateSKU() == false){
                    $_SESSION['InvalidInput'][] = 'Invalid SKU or already exists <br>';
                    header("location: addproduct.php");
                }
                if($object->validateName() == false){
                    $_SESSION['InvalidInput'][] = 'Invalid name <br>';
                    header("location: addproduct.php");
                }
                if($object->validatePrice() == false){
                    $_SESSION['InvalidInput'][] = 'Invalid price <br>';
                    header("location: addproduct.php");
                }
                if($object->validateType() == false){
                    $_SESSION['InvalidInput'][] = 'Invalid type <br>';
                    header("location: addproduct.php");
                }
                if($object->validateAttributes() == false){
                    $_SESSION['InvalidInput'][] = 'Invalid attributes <br>';
                    header("location: addproduct.php");
                }
                if(empty($_SESSION['InvalidInput'])){
                    $object->addProduct();
                }                    
        }
    }
    
if(isset($_POST['delete-submit'])){
    $products = new Query;
    if(!empty($_POST['deleteCheckbox'])) {
        foreach($_POST['deleteCheckbox'] as $sku){
            $products->deleteProduct($sku);
        }
    }else{
        header("location: index.php");
    }

}
