<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/bootstrap.css">
    <link rel="stylesheet" href="./css/custom.css">
    <script src="./js/bootstrap.js"></script>
    <title>Add-Product</title>
</head>

<body>

    <div class="custom-nav-container">
        <div class="custom-logo">
            <h1>Products Add</h1>
        </div>
        <div class="custom-buttons">
            <button type="submit" name="submit" form="product_form" class="btn btn-success">Save</button>
            <a style="text-decoration: none;" href="index.php">
                <button class="btn btn-danger">CANCEL</button>
            </a>
        </div>
    </div>
    <div class="custom-container">
        <div class="row">
            <div class="col-12">
                <?php
                    if(isset($_SESSION['InvalidInput'])){
                        foreach($_SESSION['InvalidInput'] as $productId){
                ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?php echo $productId?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                <?php 
                        } 
                        unset($_SESSION['InvalidInput']);
                    }
                ?>
            </div>
        </div>
        <form action="post.process.php" id="product_form" name="product_form" method="POST">
            <div class="row">
                <div class="col-4">
                    <div style="margin: 5px;" class="form-group row">
                        <label class="col-sm-2 col-form-label">Sku</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="sku" name="sku" placeholder="Sku">
                        </div>
                    </div>
                    <div style="margin: 5px;" class="form-group row">
                        <label class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                    </div>
                    <div style="margin: 5px;" class="form-group row">
                        <label class="col-sm-3 col-form-label">Price ($)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="price" name="price" placeholder="Price">
                        </div>
                    </div>
                    <div style="margin: 5px;" class="form-group row">
                        <label class="col-sm-4 col-form-label">Type Switcher</label>
                        <div class="col-sm-8">
                            <select onchange="changeEvent()" class="form-control" id="productType" name="productType">
                                <option value="none" selected disabled hidden>Type Switcher</option>
                                <option value="Disk">DVD</option>
                                <option value="Book">BOOK</option>
                                <option value="Furniture">FURNITURE</option>
                            </select>
                        </div>
                    </div>


                    <div id="Dvd" style="margin: 5px;" class="form-group row">
                        <label class="col-sm-4 col-form-label">Size (MB)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="size" name="size" placeholder="Size">
                        </div>
                    </div>
                    <div id="Book" style="margin: 5px;" class="form-group row">
                        <label class="col-sm-4 col-form-label">Weight (KG)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight">
                        </div>
                    </div>
                    <div id="Furniture" class="">
                        <div style="margin: 5px;" class="form-group row">
                            <label class="col-sm-4 col-form-label">Height (CM)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="height" name="height" placeholder="Height">
                            </div>
                        </div>
                        <div style="margin: 5px;" class="form-group row">
                            <label class="col-sm-4 col-form-label">Width (CM)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="width" name="width" placeholder="Width">
                            </div>
                        </div>
                        <div style="margin: 5px;" class="form-group row">
                            <label class="col-sm-4 col-form-label">Length (CM)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="length" name="length" placeholder="Length">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


    <script src="./js/custom.js"></script>
</body>

</html>