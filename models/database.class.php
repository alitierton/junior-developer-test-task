<?php
class Database{
    private $host = "localhost";
    private $user = "root";
    private $password = "";
    private $dbName = "database";

    public function connect(){
        
        $conn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
        $pdo = new PDO($conn, $this->user, $this->password);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;

    }

}
?>